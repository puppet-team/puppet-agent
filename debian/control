Source: puppet-agent
Section: admin
Priority: optional
Maintainer: Puppet Package Maintainers <pkg-puppet-devel@alioth-lists.debian.net>
Uploaders:
 Jérôme Charaoui <jerome@riseup.net>,
Build-Depends:
 debhelper-compat (= 13),
 facter (>= 4.3.0),
 gem2deb,
 pandoc <!nodoc>,
 po-debconf,
 rake <!nodoc>,
 ronn <!nodoc>,
 ruby,
 ruby-concurrent,
 ruby-deep-merge,
 ruby-fast-gettext,
 ruby-locale,
 ruby-multi-json,
 ruby-puppet-resource-api,
 ruby-scanf,
 ruby-semantic-puppet,
 yard <!nodoc>,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/puppet-team/puppet-agent.git
Vcs-Browser: https://salsa.debian.org/puppet-team/puppet-agent
Homepage: https://github.com/puppetlabs/puppet
Rules-Requires-Root: no

Package: puppet-agent
Architecture: all
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 adduser,
 libruby (>=3.3),
 ruby,
 ruby-augeas,
 ruby-shadow,
 ${misc:Depends},
 ${ruby:Depends}
Recommends:
 augeas-tools,
 debconf-utils,
 lsb-release,
 ruby-selinux,
Suggests:
 hiera-eyaml,
 ruby-hocon,
 ruby-msgpack,
Breaks:
 puppet (<< 7.16.0),
 puppet-common (<< 4.5.1-1),
 puppet-master (<< 7.9.3),
 puppet-master-passenger (<< 7.9.3),
 puppetserver (<< 8),
Replaces:
 puppet (<< 7.16.0),
 puppet-common (<< 4.5.1-1),
Description: configuration management system, agent
 Puppet is a configuration management system that allows you to define
 the state of your IT infrastructure, then automatically enforces the
 correct state.
 .
 This package contains the main Puppet libraries and the agent application.

Package: puppet-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
Suggests:
 puppet-agent,
 sensible-browser,
Description: configuration management system, docs
 Puppet is a configuration management system that allows you to define
 the state of your IT infrastructure, then automatically enforces the
 correct state.
 .
 This package contains reference documentation for Puppet and its core modules.

Package: puppet
Depends:
 puppet-agent,
 ${misc:Depends},
Architecture: all
Section: oldlibs
Description: transitional dummy package
 This is a transitional dummy package. It can safely be removed.
