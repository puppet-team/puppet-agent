require 'spec_helper'

describe "test package installation" do
  describe package('puppet-agent') do
    it { should be_installed }
  end

  describe user('puppet') do
    it { should have_home_directory '/var/lib/puppet' }
  end

  describe service('puppet') do
    it { should_not be_enabled }
    it { should_not be_running }
  end

  describe file('/var/lib/puppet') do
    it { should be_directory }
    it { should be_owned_by('puppet') }
  end

  describe command('sudo stat -c %U /var/lib/puppet/ssl') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should eq "puppet\n" }
  end

  describe file('/etc/puppet/puppet.conf') do
    it { should be_file }
  end

  describe file('/usr/share/puppet/modules') do
    it { should be_directory }
  end
end
