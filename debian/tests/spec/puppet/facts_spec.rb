require 'spec_helper'
require 'socket'

describe "test puppet facts" do
  describe command('puppet facts show') do
    its(:exit_status) { should eq 0 }
    its(:stdout_as_json) { should include('augeas') }
    its(:stdout_as_json) { should include('disks') }
    its(:stdout_as_json) { should include('memory') }
    its(:stdout_as_json) { should include('networking') }
    its(:stdout_as_json) { should include('os') }
    its(:stdout_as_json) { should include('puppetversion') }
    its(:stdout_as_json) { should include('virtual') }
  end

  describe command('puppet facts show networking.hostname') do
    its(:exit_status) { should eq 0 }
    its(:stdout_as_json) { should include('networking.hostname' => Socket.gethostname) }
  end
end
