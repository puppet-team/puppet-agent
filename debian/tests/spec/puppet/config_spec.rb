require 'spec_helper'

describe "test configuration paths" do
  describe "as regular user" do
    describe command('puppet config print') do
      its(:stdout) { should match(%r{^basemodulepath = #{ENV["HOME"]}/.puppet/code/modules:/usr/share/puppet/modules}) }
      its(:stdout) { should match(%r{^cadir = /etc/puppet/puppetserver/ca}) }
      its(:stdout) { should match(%r{^confdir = #{ENV["HOME"]}/.puppet/etc}) }
      its(:stdout) { should match(%r{^logdir = #{ENV["HOME"]}/.puppet/var/log}) }
      its(:stdout) { should match(%r{^modulepath = /usr/share/puppet/modules}) }
      its(:stdout) { should match(%r{^publicdir = #{ENV["HOME"]}/.puppet/cache/public}) }
      its(:stdout) { should match(%r{^rundir = #{ENV["HOME"]}/.puppet/var/run}) }
      its(:stdout) { should match(%r{^ssldir = #{ENV["HOME"]}/.puppet/etc/ssl}) }
      its(:stdout) { should match(%r{^reports = none}) }
      its(:stdout) { should match(%r{^vardir = #{ENV["HOME"]}/.puppet/cache}) }
      its(:stdout) { should match(%r{^vendormoduledir = /usr/share/puppet/vendor_modules}) }
    end
  end

  describe "as root" do
    describe command('sudo puppet config print') do
      its(:stdout) { should match(%r{^basemodulepath = /etc/puppet/code/modules:/usr/share/puppet/modules}) }
      its(:stdout) { should match(%r{^cadir = /etc/puppet/puppetserver/ca}) }
      its(:stdout) { should match(%r{^confdir = /etc/puppet}) }
      its(:stdout) { should match(%r{^logdir = /var/log/puppet}) }
      its(:stdout) { should match(%r{^modulepath = /usr/share/puppet/modules}) }
      its(:stdout) { should match(%r{^publicdir = /var/cache/puppet/public}) }
      its(:stdout) { should match(%r{^rundir = /run/puppet}) }
      its(:stdout) { should match(%r{^ssldir = /var/lib/puppet/ssl}) }
      its(:stdout) { should match(%r{^reports = none}) }
      its(:stdout) { should match(%r{^vardir = /var/cache/puppet}) }
      its(:stdout) { should match(%r{^vendormoduledir = /usr/share/puppet/vendor_modules}) }
    end
  end

  describe "as regular user which owns /var/lib/puppet/ssl" do
    describe command('sudo -u puppet puppet config print ssldir') do
      its(:stdout) { should eq "/var/lib/puppet/ssl\n" }
    end
  end
end
