require 'gem2deb/metadata'

@metadata = Gem2Deb::Metadata.new('.')

# load gemspec
cleaned_gemspec = @metadata.gemspec

# our gem only ships the libs
cleaned_gemspec.executables = nil
cleaned_gemspec.files = Dir['lib/**/*']

# write out new gemspec
File.open('debian/puppet.gemspec', 'w') do |file|
  file.write(cleaned_gemspec.to_ruby)
end

# write out substvars
dependencies = @metadata.get_debian_dependencies(false)
File.open("debian/puppet-agent.substvars", "a") do |fd|
  fd.puts "ruby:Depends=#{dependencies.join(', ')}"
end
