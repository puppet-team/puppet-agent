---
title: Puppet documentation
---

# References

 * [Configuration reference](references/configuration.html)
 * [Built-in function reference](references/function.html)
 * [Metaparameter reference](references/metaparameter.html)
 * [Report reference](references/report.html)
 * [Built-in types (single page)](references/type.html)

## Built-in types

[Resource types overview](references/types/overview.html)

 * [exec](references/types/exec.html)
 * [file](references/types/file.html)
 * [filebucket](references/types/filebucket.html)
 * [group](references/types/group.html)
 * [notify](references/types/notify.html)
 * [package](references/types/package.html)
 * [schedule](references/types/schedule.html)
 * [service](references/types/service.html)
 * [stage](references/types/stage.html)
 * [tidy](references/types/tidy.html)
 * [user](references/types/user.html)

# Core modules

 * [augeas_core](modules/augeas_core/reference.html)
 * [cron_core](modules/cron_core/reference.html)
 * [host_core](modules/host_core/reference.html)
 * [mount_core](modules/mount_core/reference.html)
 * [scheduled_task](modules/scheduled_task/reference.html)
 * [selinux_core](modules/selinux_core/reference.html)
 * [sshkeys_core](modules/sshkeys_core/reference.html)
 * [yumrepo_core](modules/yumrepo_core/reference.html)
 * [zfs_core](modules/zfs_core/reference.html)
 * [zone_core](modules/zone_core/reference.html)
